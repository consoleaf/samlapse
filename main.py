# This Python file uses the following encoding: utf-8
import sys
import cv2
from PySide2.QtCore import QFile
from PySide2.QtWidgets import QApplication, QMainWindow
from PySide2.QtUiTools import QUiLoader


if __name__ == "__main__":
    app = QApplication([])
    ui_file = QFile("mainwindow.ui")
    ui_file.open(QFile.ReadOnly)
    loader = QUiLoader()
    window = loader.load(ui_file)
    ui_file.close()



    window.show()

    sys.exit(app.exec_())
