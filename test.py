import random
import time

from PySide2.QtCore import *
import PySide2.QtCore
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import sys
import qimage2ndarray
import cv2
import numpy as np


class MainApp(QWidget):
    def __init__(self):
        super().__init__()
        self.prev_image = None
        self.timer = QTimer()
        self.capture = None
        self.main_layout = QVBoxLayout()
        self.quit_button = QPushButton()
        self.image_label = QLabel()
        self.stats_label = QLabel()
        self.threshold = 20
        self.threshold_slider = QSlider(PySide2.QtCore.Qt.Orientation.Horizontal)
        # self.video_size = QSize(320, 240)
        self.last_moved = time.perf_counter()
        self.diff_count = 0
        self.pics_count = 0
        self.taken_pic = True
        self.setup_ui()
        self.setup_camera()

    def setup_ui(self):
        """Initialize widgets
        """
        # self.image_label.setFixedSize(self.video_size)
        self.quit_button.clicked.connect(self.close)
        self.quit_button.setText("Close")
        self.threshold_slider.setRange(20, 255)
        self.threshold_slider.setValue(self.threshold)
        self.main_layout.addWidget(self.image_label)
        self.main_layout.addWidget(self.stats_label)
        self.main_layout.addWidget(self.threshold_slider)
        self.main_layout.addWidget(self.quit_button)

        self.setLayout(self.main_layout)

    def setup_camera(self):
        self.capture = cv2.VideoCapture(0)
        self.timer.timeout.connect(self.tick)
        self.timer.start(1000 // 30)
        # self.capture.set(cv2.CAP)

    def tick(self):
        self.threshold = self.threshold_slider.value()

        _, frame = self.capture.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.flip(frame, 1)
        diff = frame
        thresh = frame

        if self.prev_image is not None:
            # Now find out if there was any movement
            diff = cv2.subtract(self.prev_image, frame)
            self.prev_image = frame.copy()
            imgray = cv2.cvtColor(diff, cv2.COLOR_RGB2GRAY)
            ret, thresh = cv2.threshold(imgray, self.threshold, 255, 0)
            # diff = thresh
            self.diff_count = cv2.sumElems(thresh)[0]
            if self.diff_count > 0:
                self.last_moved = time.perf_counter()
                self.taken_pic = False

                ret, labels = cv2.connectedComponents(thresh)

                components = [[] for i in range(ret)]
                rectangles = [[1e9, 1e9, -1, -1] for i in range(ret)]

                nonzero = cv2.findNonZero(labels)
                for [[i, j]] in nonzero:
                    components[labels[j, i]].append([j, i])

                for i in range(len(components)):
                    component = components[i]
                    # print(component)
                    for pixel in component:
                        rectangles[i][0] = min(rectangles[i][0], pixel[1])
                        rectangles[i][1] = min(rectangles[i][1], pixel[0])
                        rectangles[i][2] = max(rectangles[i][2], pixel[1])
                        rectangles[i][3] = max(rectangles[i][3], pixel[0])

                rectangles = [rectangles[i] for i in range(ret) if rectangles[i][0] != 1e9]
                # print("Rectangles: ", rectangles)

                height, width, channels = frame.shape
                for rectangle in rectangles:
                    rectangle[0] = max(0, rectangle[0] - 50)
                    rectangle[1] = max(0, rectangle[1] - 50)
                    rectangle[2] = min(width, rectangle[2] + 50)
                    rectangle[3] = min(height, rectangle[3] + 50)

                rectangles = list(rectangles)
                rectangles, weights = cv2.groupRectangles(np.array(rectangles).tolist(), 1, 0.5)
                # print("New rectangles: ", rectangles, weights)
                for rectangle in rectangles:
                    frame = cv2.rectangle(frame, (rectangle[0], rectangle[1]), (rectangle[2], rectangle[3]),
                                          (0, 255, 0))

                # sys.exit()

                # frame = cv2.rectangle(frame, (minY, minX), (maxY, maxX), (0, 255, 0), 2)
        else:
            self.prev_image = frame.copy()

        image = qimage2ndarray.array2qimage(frame)
        self.image_label.setPixmap(QPixmap.fromImage(image))

        time_passed = time.perf_counter() - self.last_moved

        if time_passed > 3 and not self.taken_pic:
            cv2.imwrite("pic{counter}.jpg".format(counter=self.pics_count), cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
            self.pics_count += 1
            self.taken_pic = True

        self.stats_label.setText(
            """Threshold: {threshold}\nChanged count: {changed}\nTime passed not moving: {timer:.3f} seconds""".format(
                threshold=self.threshold,
                changed=self.diff_count, timer=time_passed))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = MainApp()
    win.show()
    sys.exit(app.exec_())
